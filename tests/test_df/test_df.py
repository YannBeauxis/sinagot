from pathlib import Path

import pandas as pd

from sinagot import Workspace, step, Workflow, seed, LocalStorage

from tests.utils import assert_data_equal


def test_df(shared_datadir: Path) -> None:
    @step
    def multiply(df: pd.DataFrame, factor: int) -> pd.DataFrame:
        if df.empty:
            return df
        return df * factor

    @step
    def get_single_data(df: pd.DataFrame) -> int:
        if df.empty:
            return 0
        return int(df.iloc[0, 0])

    class TestWorkflow(Workflow):
        raw_data: pd.DataFrame = seed()
        factor: int = seed()
        multiplied_data: pd.DataFrame = multiply.step(raw_data, factor=factor)
        final_data: int = get_single_data.step(multiplied_data)

    class TestWorkspace(Workspace[TestWorkflow]):
        raw_data = LocalStorage("RAW_DATA/data-{workflow_id}.csv")
        factor = LocalStorage("factor")
        multiplied_data = LocalStorage(
            "COMPUTED/step-1-{workflow_id}.csv", write_kwargs={"index": False}
        )
        final_data = LocalStorage("COMPUTED/final-{workflow_id}")

    ws = TestWorkspace(shared_datadir / "tested")

    assert ws["001"].final_data == 200

    assert_data_equal(shared_datadir / "tested", shared_datadir / "expected")

    # TODO: specific test
    ws.remote("final_data")

    assert ws["002"].final_data == 0
