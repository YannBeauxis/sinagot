from pathlib import Path
from sinagot import __version__


def test_version() -> None:
    toml_path = Path(__file__).parents[1] / "pyproject.toml"
    version_line = toml_path.read_text().split("\n")[2]
    version = version_line.split(" = ")[1]
    assert version == f'"{__version__}"'
