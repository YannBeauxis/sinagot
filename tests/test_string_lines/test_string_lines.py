from pathlib import Path

import pytest

from sinagot import Workspace, step, Workflow, seed, LocalStorage

from tests.utils import assert_data_equal


def test_string_lines(shared_datadir: Path) -> None:
    @step
    def add_one_line(str_in: str, workflow_id: str) -> str:
        return str_in + f"step1 line for {workflow_id}\n"

    @step
    def add_other_line(str_in: str) -> str:
        return str_in + "step2 line\n"

    class ExampleWorkflow(Workflow):
        raw_data: str = seed()
        intermediate_data: str = add_one_line.step(raw_data)
        final_data: str = add_other_line.step(intermediate_data)

    class ExampleWorkspace(Workspace[ExampleWorkflow]):
        raw_data = LocalStorage("RAW_DATA/input-{workflow_id}.txt")
        final_data = LocalStorage("processed/step-2-{workflow_id}.txt")

    ws = ExampleWorkspace(shared_datadir / "tested")

    workflow = ws["001"]

    assert (
        workflow.raw_data
        == (shared_datadir / "expected/RAW_DATA/input-001.txt").read_text()
    )

    assert workflow.intermediate_data == "raw data 001\nstep1 line for 001\n"

    assert (
        workflow.final_data
        == (shared_datadir / "expected/processed/step-2-001.txt").read_text()
    )

    [workflow.final_data for workflow in ws.values()]

    assert_data_equal(shared_datadir / "tested", shared_datadir / "expected")

    assert repr(workflow) == "ExampleWorkflow('001')"
    assert repr(ExampleWorkflow.__dict__["raw_data"]) == "Seed(raw_data)"
    assert repr(ExampleWorkflow.__dict__["final_data"]) == "Step(final_data)"

    with pytest.raises(AttributeError):
        ws["001"].WORKFLOW_ID_PATTERN  # type: ignore

    assert len(ws) == 3

    assert set(ws) == {"001", "022", "333"}
