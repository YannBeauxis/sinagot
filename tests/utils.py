import os
from pathlib import Path
from typing import Generator


def iter_files(path: Path) -> Generator[Path, None, None]:
    return (
        Path(root, file_).relative_to(path)
        for root, dirs, files in os.walk(path)
        for file_ in files
    )


def assert_data_equal(tested: Path, expected: Path) -> None:
    paths = (tested, expected)
    for path_a, path_b in [paths, reversed(paths)]:
        for file_path in iter_files(path_a):
            file_path_a = path_a / file_path
            file_path_b = path_b / file_path
            assert file_path_b.exists()
            assert file_path_b.read_bytes() == file_path_a.read_bytes()
