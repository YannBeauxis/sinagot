import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8 sinagot tests"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict sinagot tests"
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = "pytest --cov-report term-missing --cov=sinagot --disable-warnings"
    subprocess.run(shlex.split(cmd))
