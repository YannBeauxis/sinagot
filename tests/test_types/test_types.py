from pathlib import Path

import pytest

from sinagot import Workspace, Workflow, seed, LocalStorage


def test_format(shared_datadir: Path) -> None:
    class TestWorkflow(Workflow):
        raw_data: float = seed()

    class TestWorkspace(Workspace[TestWorkflow]):
        raw_data = LocalStorage(
            "data-{workflow_id}",
        )

    ws = TestWorkspace(shared_datadir / "tested")

    workflow = ws["001"]
    with pytest.raises(NotImplementedError):
        workflow.raw_data
