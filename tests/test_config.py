import logging

import pytest

from sinagot import Workspace, Workflow
from sinagot.config import get_settings
from sinagot.logger import get_logger


@pytest.mark.parametrize(
    "level", ("NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
)
def test_log_settings(level: str, monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setenv("SINAGOT_LOGGING_LEVEL", level)
    settings = get_settings()
    assert settings.LOGGING_LEVEL == level

    Workspace[Workflow]()
    logger = get_logger()
    assert logger.level == getattr(logging, level)
