from sinagot.path_template import PathTemplate


def test_path_template() -> None:
    pt = PathTemplate("foo/{bar}")
    assert repr(pt) == "PathTemplate('foo/{bar}')"

    assert pt.format_keys() == {"bar"}
