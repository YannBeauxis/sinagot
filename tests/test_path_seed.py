from pathlib import Path

import sinagot as sg


def test_path_seed() -> None:

    ROOT_PATH = "/root"
    TEST_PATH = "test/path"

    @sg.step
    def convert_path_to_str(path: Path) -> str:
        return str(path)

    class TestWokflow(sg.Workflow):

        path: Path = sg.seed()
        path_str = convert_path_to_str.step(path=path)

    class TestWorkspace(sg.Workspace[TestWokflow]):

        path = sg.LocalStorage(TEST_PATH)

    ws = TestWorkspace(ROOT_PATH)

    assert ws["1"].path_str == str(Path(ROOT_PATH, TEST_PATH))
